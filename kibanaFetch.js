"use strict";
//https://ezmesure.couperin.org/api-reference
// rubrique indices
// API cible Indices/ GET /logs/{index}/aggregation.{extension}.
// Cette api répond à mes besoins puisqu'on peut choisir les champs que l'on requête (rtype,mime,platform) et borner une période, mais elle renvoit du ndjson au lieu de json.
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
require("dotenv").config({ path: "./config/.env" });
const token = process.env.API_KEY;
axios_1.default.defaults.headers.common = { Authorization: `Bearer ${token}` };
const instance = axios_1.default.create({
    baseURL: "https://ezmesure.couperin.org/api",
    headers: { Authorization: "Bearer " + token },
});
const kibanaFetcher = (startDate, endDate) => __awaiter(void 0, void 0, void 0, function* () {
    //API : INDICES GET /logs/{index}/aggregation
    // fields to fetch : rtype,mime,platform
    const response = yield instance.get(`/logs/univ-pau/aggregation.ndjson?fields=platform&missing=true&from=${startDate}&to=${endDate}&filter=status%3A200&delimiter=%2C`);
    const data = yield response.data;
    console.log(typeof data);
    console.log(data);
    let ndJsonStringSplit = yield data.split("\n");
    // il reste deux lignes blanches dans l'array, à ejecter avant de parser
    ndJsonStringSplit.pop();
    ndJsonStringSplit.pop();
    let test2 = [];
    for (let s of ndJsonStringSplit) {
        test2.push(JSON.parse(s));
    }
    return test2;
});
const run = () => __awaiter(void 0, void 0, void 0, function* () {
    console.log(yield kibanaFetcher("2020-07-01", "2021-07-01"));
});
run();
