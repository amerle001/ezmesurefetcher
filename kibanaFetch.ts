//https://ezmesure.couperin.org/api-reference
// rubrique indices
// API cible Indices/ GET /logs/{index}/aggregation.{extension}.
// Cette api répond à mes besoins puisqu'on peut choisir les champs que l'on requête (rtype,mime,platform) et borner une période, mais elle renvoit du ndjson au lieu de json.

import axios from "axios";
require("dotenv").config({ path: "./config/.env" });

const token = process.env.API_KEY;

axios.defaults.headers.common = { Authorization: `Bearer ${token}` };

const instance = axios.create({
  baseURL: "https://ezmesure.couperin.org/api",
  headers: { Authorization: "Bearer " + token },
});

const kibanaFetcher = async (
  startDate: string,
  endDate: string
): Promise<Object[]> => {
  //API : INDICES GET /logs/{index}/aggregation
  // fields to fetch : rtype,mime,platform
  const response = await instance.get(
    `/logs/univ-pau/aggregation.ndjson?fields=platform&missing=true&from=${startDate}&to=${endDate}&filter=status%3A200&delimiter=%2C`
  );

  const data = await response.data;
  console.log(typeof data);

  console.log(data);

  let ndJsonStringSplit: [string] = await data.split("\n");

  // il reste deux lignes blanches dans l'array, à ejecter avant de parser
  ndJsonStringSplit.pop();
  ndJsonStringSplit.pop();

  let result: Array<Object> = [];
  for (let s of ndJsonStringSplit) {
    result.push(JSON.parse(s));
  }

  return result;
};

const run = async () => {
  console.log(await kibanaFetcher("2020-07-01", "2021-07-01"));
};

run();
